/**
 * asp_therm - implementation of real gas equations of state
 * ===================================================================
 * * gas_mmix_average_parameters *
 *   В модуле реализован расчёт средних параметров смеси по разным методологиям.
 * ===================================================================
 *
 * Copyright (c) 2020-2021 Mishutinski Yurii
 *
 * This library is distributed under the MIT License.
 * See LICENSE file in the project root for full license information.
 */

#ifndef _CORE__GAS_PARAMETERS__GAS_MIX_AVERAGE_PARAMETERS_H
#define _CORE__GAS_PARAMETERS__GAS_MIX_AVERAGE_PARAMETERS_H

#include "gas_description.h"

/**
 * \brief Функции расчёта средних параметров по методам из
 *   книги "Свойства газов и жидкостей" Рида, Праусница, Шервуда
 * */
namespace ns_avg {
/* методика применяемая к классической модели Редлиха-Квонга */
/** \brief Рассчитать среднюю критическую температуру по
  *   методу Редлиха-Квонга(двухпараметрическому, глава 4.3) */
double rk2_avg_Tk(const parameters_mix &components);
/** \brief Рассчитать среднее критическое давление по
  *   методу Редлиха-Квонга(двухпараметрическому, глава 4.3) */
double rk2_avg_Pk(const parameters_mix &components);
/** \brief Параметр сжимаемости в критической точке -
  *   для модели Редлиха Квонга равен 1/3 */
double rk2_avg_Zk();
/** \brief Рассчитать среднее значение фактора ацентричности(глава 4.2) */
/* todo: такс, вероятно, там объёмная доля, а не молярная  */
double rk2_avg_acentric(const parameters_mix &components);

// "истинные" параметры критической точки
/** \brief Рассчитать среднюю критическую температуру по
  *   методу Ли (глава 5.7):
  * PSY_i = y_i * Vk_i / SUM_j (y_j * Vk_j)
  * Tk = SUM_i (PSY_i * Tk_i) */
double lee_avg_Tk(const parameters_mix &components);
/** \brief Рассчитать среднюю критическую температуру по
  *   методу Чью-Праусница (глава 5.7):
  * TETA_i = y_i * Vk_i^0.66(6) / SUM_j (y_j * Vk_j^0.66(6))
  * Tk = SUM_i (TETA_i * Tk_i) + SUM_i (SUM_j (TETA_i * TETA_j  * t_i_j))
  *   t_i_j = 0 для i = j
  *   иначе:
  *   psy = A + B*d + C*d^2 + D*d^3 + E*d^4,
  *   also: psy = 2 * t_i_j / (Tk_i + Tk_j)
  *   and: d = |(Tk_i - Tk_j) / (Tk_i + Tk_j)| */
double ch_pr_avg_Tk(const parameters_mix &components);
/** \brief Рассчитать среднй критический объём смеси по
  *   методу Чью-Праусница (глава 5.7):
  * TETA_i = y_i * Vk_i^0.66(6) / SUM_j (y_j * Vk_j^0.66(6))
  * Vk = SUM_i (TETA_i * Vk_i) + SUM_i (SUM_j (TETA_i * TETA_j  * v_i_j))
  *   v_i_j = 0 для i = j
  *   иначе:
  *   psy = A + B*d + C*d^2 + D*d^3 + E*d^4,
  *   also: psy = 2 * v_i_j / (Vk_i + Vk_j)
  *   and: d = |(Vk_i^0.6667 - Vk_j^0.6667) / (Vk_i^0.6667 + Vk_j^0.6667)| */
double ch_pr_avg_Vk(const parameters_mix &components);

constexpr int index_pk = 0;
constexpr int index_tk = 1;
constexpr int index_vk = 2;
constexpr int index_zk = 3;
constexpr int index_mol = 4;
constexpr int index_accent = 5;
/** \brief Получить массив средних значений(глава 4.2)
 *   [P_k, T_k, V_k, Z_k, mol, acentric]*/
std::array<double, 6> get_average_params(const parameters_mix& components,
                                         const model_str& ms);
/* todo: вычисление критического давления по книге РПШ
 *   сложно и не понятно */
}  // namespace ns_avg


#endif  // !_CORE__GAS_PARAMETERS__GAS_MIX_AVERAGE_PARAMETERS_H
