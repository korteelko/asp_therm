/**
 * asp_therm - implementation of real gas equations of state
 *
 *
 * Copyright (c) 2020-2021 Mishutinski Yurii
 *
 * This library is distributed under the MIT License.
 * See LICENSE file in the project root for full license information.
 */
#include "gas_description_mix.h"

#include "asp_utils/ErrorWrap.h"
#include "asp_utils/Logging.h"
#include "atherm_common.h"
#include "model_general.h"
#include "gas_mix_average_parameters.h"

#include <algorithm>
#include <cmath>
#include <numeric>
#include <vector>

#include <assert.h>
#include <string.h>

mix_component_info::mix_component_info(const std::string& name,
                                       const std::string& path,
                                       const double part)
    : name(name), path(path), part(part) {}

bool operator<(const mix_component_info& lg,
               const mix_component_info& rg) {
  return strcmp(lg.name.c_str(), rg.name.c_str()) <= 0;
}


// GasParameters_mix
GasParameters_mix::GasParameters_mix(parameters prs,
                                     const_parameters cgp,
                                     dyn_parameters dgp,
                                     parameters_mix components)
    : GasParameters(prs, cgp, dgp), components_(components) {}

GasParameters_mix::~GasParameters_mix() {}

GasParameters_mix_dyn::GasParameters_mix_dyn(parameters prs,
                                             const_parameters cgp,
                                             dyn_parameters dgp,
                                             parameters_mix components,
                                             modelGeneral* mg)
    : GasParameters_mix(prs, cgp, dgp, components),
      prev_vpte_(prs),
      model_(mg) {}


/**
 * \brief Рассчитать средние критические параметры смеси
 */
auto make_average_const_parameters(const auto& components,
                                   const auto& model_str) {
   const auto avr_vals =
      ns_avg::get_average_params(components, model_str);
  return std::unique_ptr<const_parameters>(const_parameters::Init(
      GAS_TYPE_MIX, avr_vals[ns_avg::index_vk], avr_vals[ns_avg::index_pk],
      avr_vals[ns_avg::index_tk], avr_vals[ns_avg::index_zk],
      avr_vals[ns_avg::index_mol], avr_vals[ns_avg::index_accent]));
}

GasParameters_mix_dyn* GasParameters_mix_dyn::Init(gas_params_input gpi,
                                                   modelGeneral* mg) {
  GasParameters_mix_dyn* mix = nullptr;
  if (!gpi.const_dyn.components->empty() && (mg != nullptr)) {
    const auto tmp_cgp = make_average_const_parameters(*gpi.const_dyn.components,
                                                 mg->GetModelShortInfo());
    if (tmp_cgp.get()) {
      mix = new GasParameters_mix_dyn({0.0, gpi.p, gpi.t}, *tmp_cgp,
                                      dyn_parameters(),
                                      *gpi.const_dyn.components, mg);
    } else {
      Logging::Append(ERROR_PAIR_DEFAULT(ERROR_CALC_GAS_P_ST));
      Logging::Append(io_loglvl::debug_logs,
                      "Пустой список компонентов газовой смеси ");
    }
  } else {
    Logging::Append(ERROR_PAIR_DEFAULT(ERROR_INIT_NULLP_ST));
    Logging::Append(io_loglvl::debug_logs,
                    "Пустой список компонентов газовой смеси "
                    "или не инициализирована модель");
  }
  return mix;
}

void GasParameters_mix_dyn::InitDynamicParams() {
  /*
  dyn_setup setup = DYNAMIC_SETUP_MASK;
  double volume = 0.0;
  std::vector<std::pair<double, dyn_parameters>> dgp_cpt;
  model_->update_dyn_params(dgp_cpt.back().second,
    { volume, gpi.p, gpi.t}, x.second.first);
  std::array<double, 3> dgp_tmp = {0.0, 0.0, 0.0};
  for (auto const &x : dgp_cpt) {
    dgp_tmp[0] += x.first * x.second.heat_cap_vol;
    dgp_tmp[1] += x.first * x.second.heat_cap_pres;
    dgp_tmp[2] += x.first * x.second.internal_energy;
    setup &= x.second.setup;
  }
  tmp_dgp = std::unique_ptr<dyn_parameters>(dyn_parameters::Init(setup,
      dgp_tmp[0], dgp_tmp[1], dgp_tmp[2], {volume, gpi.p, gpi.t}));
  */
  status_ = STATUS_OK;
}

std::unique_ptr<const_parameters> GasParameters_mix_dyn::GetAverageParams(
    parameters_mix& components,
    const model_str& mi) {
  std::unique_ptr<const_parameters> tmp_cgp = nullptr;
  if (!components.empty()) {
    const auto avr_vals = ns_avg::get_average_params(components, mi);
    // init gasmix const_parameters
    if (!(tmp_cgp = std::unique_ptr<const_parameters>(const_parameters::Init(
              GAS_TYPE_MIX, avr_vals[ns_avg::index_vk],
              avr_vals[ns_avg::index_pk], avr_vals[ns_avg::index_tk],
              avr_vals[ns_avg::index_zk], avr_vals[ns_avg::index_mol],
              avr_vals[ns_avg::index_accent]))))
      Logging::Append(ERROR_CALC_GAS_P_ST,
                      "Расчёт средних параметров для газовой смеси");
  } else {
    Logging::Append(ERROR_INIT_NULLP_ST,
                    "Инициализация газовой смеси компонентов нет");
  }
  return tmp_cgp;
}

const parameters_mix& GasParameters_mix_dyn::GetComponents() const {
  return components_;
}

void GasParameters_mix_dyn::csetParameters(double v,
                                           double p,
                                           double t,
                                           state_phase sp) {
  std::swap(prev_vpte_, vpte_);
  vpte_.volume = v;
  vpte_.pressure = p;
  vpte_.temperature = t;
  sph_ = sp;
  /*
  assert(0 && "удалить обновление динамических параметров или "
      "сделать правильно - в зависимости от используемой модели. "
      "не таким методом - не для каждлго компонента отдельно");
  // todo: здесь не совсем правильно -
  //   модель выставлена для смеси(скорее всего),
  //   а используется для компонентов отдельно!
  for (auto &x : components_)
    model_->update_dyn_params(x.second.second, vpte_);
  dyn_params_.heat_cap_vol  = 0.0;
  dyn_params_.heat_cap_pres = 0.0;
  dyn_params_.internal_energy = 0.0;
  dyn_params_.beta_kr = 0.0;
  dyn_params_.parm = vpte_;
  for (auto const &x : components_) {
    dyn_params_.heat_cap_vol  += x.first * x.second.second.heat_cap_vol;
    dyn_params_.heat_cap_pres += x.first * x.second.second.heat_cap_pres;
    dyn_params_.internal_energy +=
        x.first * x.second.second.internal_energy;
  }
  */
}
