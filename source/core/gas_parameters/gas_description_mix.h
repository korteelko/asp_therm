/**
 * asp_therm - implementation of real gas equations of state
 *
 *
 * Copyright (c) 2020-2021 Mishutinski Yurii
 *
 * This library is distributed under the MIT License.
 * See LICENSE file in the project root for full license information.
 */
#ifndef _CORE__GAS_PARAMETERS__GAS_DESCRIPTION_MIX_H
#define _CORE__GAS_PARAMETERS__GAS_DESCRIPTION_MIX_H

#include "gas_description_static.h"

// Не имеет смысла определять все составляющие газовой
//   смеси. 97% составляющих будет достаточно
// И не забудем ошибку вычисления, или округления,
//   отведём ей 1%
// Итого проверяем тождество :
//   |0.99 - ${sum_of_components}| < 0.03
#define GASMIX_PERSENT_AVR  0.99
#define GASMIX_PERCENT_EPS  0.03

/**
 * \brief Параметры компонента газовой смеси описанные
 *   в файле инициализации смеси
 * */
struct mix_component_info {
  std::string name;
  std::string path;
  double part;

public:
 mix_component_info(const std::string &name,
      const std::string &path, double part);
};
bool operator<(const mix_component_info&lg, const mix_component_info&rg);


/* todo: remove this class */
class GasParameters_mix : public GasParameters {
protected:
  parameters_mix components_;

protected:
  GasParameters_mix(parameters prs, const_parameters cgp,
      dyn_parameters dgp, parameters_mix components);
  virtual ~GasParameters_mix();
};

/** \brief класс инкапсулирующий работы с теплофизическими
  *   параметрами смеси газов.
  * Для смеси выставляются такие параметры(y_i - молярная доля компонента):
  *   mol = SUM y_i * mol_i  - молярная масса
  *   R = Rm / mol - газовая постоянная.
  * Для других параметров(например критических t, p)
  *   существует несколько методик расчёта, см код
  * Также, в большинстве моделей оперируют не средними параметрами,
  *   а параметрами компонентов */
class GasParameters_mix_dyn final: public GasParameters_mix {
  // previous pressure, volume and temperature
  parameters prev_vpte_;
  /** \brief обратный указатель на модель, для использования
    *   функций специализированных в модели */
  modelGeneral *model_;

private:
  GasParameters_mix_dyn(parameters prs, const_parameters cgp,
      dyn_parameters dgp, parameters_mix components, modelGeneral *mg);

public:
  static GasParameters_mix_dyn *Init(gas_params_input gpi, modelGeneral *mg);
  //  неправильно, средние параметры зависят от модели
  static std::unique_ptr<const_parameters> GetAverageParams(
      parameters_mix &components, const model_str &mi);

  void InitDynamicParams();
  const parameters_mix &GetComponents() const;
  void csetParameters(double v, double p, double t, state_phase sp) override;
};
#endif  // !_CORE__GAS_PARAMETERS__GAS_DESCRIPTION_MIX_H
